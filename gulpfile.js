const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify');
const minify = require('gulp-css');
const reload = browserSync.reload;
//convert scss to css
function style() {
    return gulp.src('./scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./dist/css/'))
    .pipe(browserSync.stream());
}
//handle js files
function js() {
    return gulp.src('./js/**/*.js')
    .pipe(gulp.dest('./dist/js/'))
    .pipe(browserSync.stream())
    .pipe(reload);
}
//create folders
 function makedir() {
    return gulp.src('*.*', {read: false})
        .pipe(gulp.dest('./dist/css/cssmin/'))
        .pipe(gulp.dest('./dist/img/content'))
        .pipe(gulp.dest('./dist/img/icons'))
        .pipe(gulp.dest('./dist/fonts'))
        .pipe(gulp.dest('./dist/js'));
};

function smallCss() {
   return gulp.src('./dist/css/**/*.css')
   .pipe(minify())
   .pipe(gulp.dest('./dist/css/cssmin/'))
}

function ugli() {
   return gulp.src('./js/**/*.js')
   .pipe(uglify())
   .pipe(gulp.dest('./dist/js/jsmin/'))
}

function watch() {
    browserSync.init({
        server: {
            baseDir: './'
        }
    })
    gulp.watch('./scss/**/*.scss', style);
    gulp.watch('./components/**/*.scss', style);
    gulp.watch('./*.html').on('change', reload);
    gulp.watch('./js/**/*.js',  js);
}


exports.style = style;
exports.watch = watch;
exports.makedir = makedir;
exports.smallCss = smallCss;
exports.ugli = ugli;
exports.css = (style,smallCss);
exports.default = watch;